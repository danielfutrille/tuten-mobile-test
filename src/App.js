import { Route } from 'react-router-dom';
import React from "react";
import { IonApp, IonRouterOutlet } from '@ionic/react';
import { IonReactRouter } from '@ionic/react-router';
import Login from './pages/Login';
import { Plugins, Capacitor } from "@capacitor/core";

/* Core CSS required for Ionic components to work properly */
import '@ionic/react/css/core.css';

/* Basic CSS for apps built with Ionic */
import '@ionic/react/css/normalize.css';
import '@ionic/react/css/structure.css';
import '@ionic/react/css/typography.css';

/* Optional CSS utils that can be commented out */
import '@ionic/react/css/padding.css';
import '@ionic/react/css/float-elements.css';
import '@ionic/react/css/text-alignment.css';
import '@ionic/react/css/text-transformation.css';
import '@ionic/react/css/flex-utils.css';
import '@ionic/react/css/display.css';

/* Theme variables */
import './theme/variables.css';
import Bookings from './pages/Bookings';
import Auth from './Auth';

class App extends React.Component {
    constructor(props) {
        super(props);

        this.state = {};
    }

    componentDidMount() {
        if (Capacitor.isNative) {
            Plugins.App.addListener("backButton", (e) => {
                if (window.location.pathname === "/") {
                    // Show A Confirm Box For User to exit app or not
                    Plugins.App.exitApp();
                } else if (window.location.pathname === "/bookings") {
                    // Show A Confirm Box For User to exit app or not
                    let ans = window.confirm("Are you sure");
                    if (ans) {
                        Plugins.App.exitApp();
                    }
                }
            });
        }
    }

    render() {
        return (
            <IonApp >
                <IonReactRouter>
                    <IonRouterOutlet>
                        <Route exact path="/" render={props => {
                            return Auth.getIsAuthenticated() ? <Bookings {...props} /> : <Login />;
                        }} />
                        <Route exact path="/bookings" render={props => {
                            return Auth.getIsAuthenticated() ? <Bookings {...props} /> : <Login />;
                        }} />
                    </IonRouterOutlet>
                </IonReactRouter>
            </IonApp >
        );
    }
}

export default App;
