
export function isNullOrUndefined(value) {
    return null === value || typeof value === 'undefined';
}


export function log(...data) {
    if (process.env.REACT_APP_ENV !== "production") {
        console.log("[dev]", ...data);
    }
}
