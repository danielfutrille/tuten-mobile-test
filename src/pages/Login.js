import {
    IonButton,
    IonCard,
    IonCardContent,
    IonCardHeader,
    IonCardSubtitle,
    IonCardTitle,
    IonContent,
    IonHeader,
    IonIcon,
    IonInput,
    IonItem,
    IonLabel,
    IonPage,
    IonTitle,
    IonToolbar
} from "@ionic/react";
import { pin, walk, warning, wifi, wine } from "ionicons/icons";
import React from "react";
import { connect } from "react-redux";
import "./Login.css";
import { signIn } from '../routes/userRoutes';
import Auth from "../Auth";
import { Redirect } from "react-router";

class Login extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            email: '',
            password: '',
        };

        this.onChange = this.onChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }

    onSubmit(e) {
        e.preventDefault();
        e.stopPropagation();

        if (this.state.email !== '' && this.state.password !== '') {
            this.props.signIn(this.state.email, this.state.password, null)
                .then((response) => {
                    this.checkLogin(response.data);
                }, (errors) => {
                    console.log(errors);
                })
                .catch((error) => {
                    console.log('[CatchError]', Object.keys(error));
                });
        }
    }

    checkLogin = (data) => {
        Auth.login(data, () => {
            this.setState(() => ({
                authenticated: true
            }));
        });
    };

    onChange(e) {
        this.setState({ [e.target.name]: e.target.value });
    }

    render() {
        const {
            email,
            password,
        } = this.state;
        
        if (Auth.getIsAuthenticated()) {
            return (
                <Redirect to="/bookings" />
            )
        }

        return (
            <IonPage>
                <IonHeader>
                    <IonToolbar color="primary">
                        <IonTitle>Tuten MOBILE Test</IonTitle>
                    </IonToolbar>
                </IonHeader>

                <IonContent className="ion-padding">
                    <IonCard>

                        <IonCardContent>
                            <IonItem>
                                <IonLabel position="stacked">Correo Electrónico</IonLabel>
                                <IonInput value={email} type="email" name="email" onIonChange={this.onChange}> </IonInput>
                            </IonItem>
                            <IonItem>
                                <IonLabel position="stacked">Contraseña</IonLabel>
                                <IonInput value={password} type="password" name="password" onIonChange={this.onChange}> </IonInput>
                            </IonItem>
                        </IonCardContent>

                        <IonCardContent>
                            <IonButton fill="solid" expand="block" onClick={this.onSubmit}>Entrar</IonButton>
                        </IonCardContent>
                    </IonCard>

                </IonContent>
            </IonPage>
        );
    }
}

// export default Login;

function mapStateToProps(globalState) {
    return {
    }
}

const mapDispatchToProps = {
    signIn,
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);
