import {
    IonButton,
    IonCard,
    IonCardContent,
    IonCardHeader,
    IonCardSubtitle,
    IonCardTitle,
    IonCol,
    IonContent,
    IonGrid,
    IonHeader,
    IonIcon,
    IonInput,
    IonItem,
    IonLabel,
    IonList,
    IonListHeader,
    IonNote,
    IonPage,
    IonRadio,
    IonRadioGroup,
    IonRow,
    IonSearchbar,
    IonSegment,
    IonSegmentButton,
    IonText,
    IonTitle,
    IonToolbar,
} from "@ionic/react";
import React, { Fragment } from "react";
import { connect } from "react-redux";
import {
    bookmark,
    calculatorOutline,
    call,
    camera,
    heart,
    heartOutline,
    pinOutline,
    refreshOutline,
} from "ionicons/icons";
import { pin, wifi, wine, warning, walk } from "ionicons/icons";
import "./Bookings.css";
import Auth from "../Auth";
import { Redirect } from "react-router";
import { getBookings } from '../routes/userRoutes';
import NumberFormat from 'react-number-format';


class Bookings extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            authenticated: Auth.getIsAuthenticated(),
            bookings: [],
            filterType: '',
            filterBookingId: '',
            filterPriceOperator: '>=',
            filterPrice: '',
        };
        this.onChange = this.onChange.bind(this);
        this.logout = this.logout.bind(this);
        this.changeFilterType = this.changeFilterType.bind(this);
        this.changeFilterPriceOperator = this.changeFilterPriceOperator.bind(this);
    }

    onChange(e) {
        this.setState({ [e.target.name]: e.target.value });
    }

    changeFilterType(newType) {
        this.setState({ filterType: newType });
    }

    changeFilterPriceOperator() {
        this.setState({ filterPriceOperator: this.state.filterPriceOperator === '>=' ? '<=' : '>=' });
    }

    logout(e) {
        e.preventDefault();
        e.stopPropagation();

        Auth.logout();
        this.setState({ authenticated: false });
    }

    componentDidMount() {
        this.getBookings();
    }

    getBookings() {
        // this.setState({ isLoading: true });
        this.props.getBookings('contacto@tuten.cl')
            .then((response) => {
                this.setState({
                    // isLoading: false,
                    bookings: response.data
                });

            }, (errors) => {
                // this.setState({ isLoading: false });
                console.log(errors);
            })
            .catch((error) => {
                console.log('[CatchError]', Object.keys(error));
            });
    }

    render() {
        const {
            filterBookingId,
            authenticated,
            bookings,
            filterType,
            filterPrice,
            filterPriceOperator,
        } = this.state;


        let filteredBookings = bookings;

        if (filterBookingId !== '' || filterPrice !== '') {
            filteredBookings = bookings.filter((_b) => {
                return (
                    (filterType === 'booking' && _b.bookingId.toString().indexOf(filterBookingId) > -1)
                    ||
                    (filterType === 'price' && (
                        (filterPriceOperator === '>=' && _b.bookingPrice >= parseFloat(filterPrice))
                        ||
                        (filterPriceOperator === '<=' && _b.bookingPrice <= parseFloat(filterPrice))
                    ))
                );
            });
        }

        const children = filteredBookings.map((_booking, key) => {
            return (
                <BookingItem {...this.props}
                    key={_booking.id + '-' + key}
                    booking={_booking} />
            );
        });


        if (!authenticated) {
            return (
                <Redirect to="/" />
            )
        }
        return (
            <IonPage>
                <IonHeader>
                    <IonToolbar color="primary">
                        <IonTitle>Tuten MOBILE Test</IonTitle>

                        <IonButton color="light" fill="outline" slot="end" onClick={this.logout} >Salir</IonButton>
                    </IonToolbar>
                </IonHeader>

                <IonContent className="ion-padding">
                    <IonTitle color="primary" size="large" class="ion-text-center" >Bookings</IonTitle>
                    <IonLabel class="ion-padding-horizontal">Filtrar por:</IonLabel>
                    <IonSegment class="ion-padding-horizontal"
                        onIonChange={e => this.changeFilterType(e.detail.value)}>
                        <IonSegmentButton value="booking">
                            <IonLabel>Booking Id</IonLabel>
                        </IonSegmentButton>
                        <IonSegmentButton value="price">
                            <IonLabel>Precio</IonLabel>
                        </IonSegmentButton>
                    </IonSegment>
                    {filterType === 'booking' &&
                        <IonInput
                            class="ion-padding-horizontal custom-border"
                            value={filterBookingId}
                            type="text"
                            name="filterBookingId"
                            onIonChange={this.onChange}
                            placeholder="BookingId"> </IonInput>
                    }
                    {filterType === 'price' &&
                        <Fragment>
                            <IonRadioGroup value={filterPriceOperator} onIonChange={e => this.changeFilterPriceOperator(e.detail.value)}>
                                <IonItem>
                                    <IonLabel>&gt;=</IonLabel>
                                    <IonRadio slot="start" value=">=" />
                                </IonItem>

                                <IonItem>
                                    <IonLabel>&lt;=</IonLabel>
                                    <IonRadio slot="start" value="<=" />
                                </IonItem>
                            </IonRadioGroup>
                            <IonInput
                                class="ion-padding-horizontal custom-border"
                                value={filterPrice}
                                type="number"
                                name="filterPrice"
                                onIonChange={this.onChange}
                                placeholder="Precio"></IonInput>
                        </Fragment>
                    }
                    <IonGrid>

                        <IonList>{children}</IonList>

                    </IonGrid>

                    {/* <IonList>{children}</IonList> */}
                </IonContent>
            </IonPage>
        );
    }
}

export const BookingItem = (props) => {

    const { booking } = props;
    let date = new Date(booking.bookingTime);

    const formatted = ('0' + date.getUTCDay()).slice(-2) + '-' + ('0' + date.getUTCMonth()).slice(-2) + '-' + date.getUTCFullYear();

    return (

        <IonItem>
            <IonCard class="card-center">
                <IonCardContent>
                    <IonItem>
                        <IonLabel color="primary">BookingId:</IonLabel>

                        <IonLabel className="ion-text-wrap">
                            {booking.bookingId}
                        </IonLabel>
                    </IonItem>
                    <IonItem>
                        <IonLabel color="dark">Cliente:</IonLabel>

                        <IonLabel className="ion-text-wrap">
                            {booking.tutenUserClient.firstName + ' ' + booking.tutenUserClient.lastName}
                        </IonLabel>
                    </IonItem>
                    <IonItem>
                        <IonLabel color="dark" className="ion-text-wrap">Fecha de Creación:</IonLabel>

                        <IonLabel className="ion-text-wrap">
                            {formatted}
                        </IonLabel>
                    </IonItem>
                    <IonItem>
                        <IonLabel color="dark">Dirección:</IonLabel>

                        <IonLabel className="ion-text-wrap">
                            {booking.locationId.streetAddress}
                        </IonLabel>
                    </IonItem>
                    <IonItem>
                        <IonLabel color="dark">Precio:</IonLabel>

                        <IonLabel className="ion-text-wrap">
                            <NumberFormat
                                id="bookingPrice"
                                displayType="text"
                                type="text"
                                value={booking.bookingPrice}
                                name="bookingPrice"
                                thousandSeparator={"."}
                                decimalSeparator={","}
                                decimalScale={2}
                                fixedDecimalScale={true}
                            />
                        </IonLabel>
                    </IonItem>
                </IonCardContent>
            </IonCard>
        </IonItem>
    );
}

function mapStateToProps(globalState) {
    return {
    }
}

const mapDispatchToProps = {
    getBookings,
};

export default connect(mapStateToProps, mapDispatchToProps)(Bookings);
