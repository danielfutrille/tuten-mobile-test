# Tuten Mobile Test

## Pre requisitos y Versiones
- La App fue creada en `Ionic 5` con React
- Version de NPM: `6.14.18`
- Versión de Node: `14.15.1`
- Para Generar APK, Android Studio (Opcional): `4.1.3.0`

## Pasos para instalación
- Clonar repositorio desde: [https://gitlab.com/danielfutrille/tuten-mobile-test.git](https://gitlab.com/danielfutrille/tuten-mobile-test.git)
- Desde el directorio raiz (tuten-mobile-test) ejecutar el siguiente comando: `npm install`
- Al finalizar, ejecutar el siguiente comando: `ionic serve`

El servicio se iniciará por defecto en `http://localhost:8100`. Lo mostrado es una versión Web de la App Móvil. Se recomienda instalar el APK -->

## APK DESCARGABLE
- Descargar e instalar el APK desde la siguiente URL: [https://drive.google.com/file/d/1jV6Z6k5yTsuJajK8hJn8OGDBZGWGtdR_/view?usp=sharing](https://drive.google.com/file/d/1jV6Z6k5yTsuJajK8hJn8OGDBZGWGtdR_/view?usp=sharing)
- **Nota:** dar los permisos para instalación de aplicaciones que no provienen desde Play Store.

## Generar APK (Opcional)
Si se desea generar el APK, seguir los siguientes pasos:
- Generar paquete de ionic ejecutando: `ionic build`
- Agregar soporte de Capacitor para Android: `npx cap add android`
- Abrir Android Studio para sincronización: `npx cap open android`. Para evitar un posible inconveniente se debe agregar la ruta de instalación del Android Studio `"linuxAndroidStudioPath": "/snap/bin/android-studio"` en el archivo `capacitor.config.json` ubicado en la raíz del proyecto.
- Esperar que Android Studio abra y sincronice con Gradle
- Ir al menu `Build -> Generate Signed Bundle / APK...`
- Seleccionar opción `APK` y "Next"
- Crear o utilizar un KeyFile existente y "Next".
- Seleccionar opción `release` y marcar `V2 (Full APK Signature)` y "Finish"
- El APK se generará en la siguiente ruta desde la raíz del proyecto: `/android/app/release/app-release.apk`


## Consideraciones
- Las mismas del proyecto UI en [https://gitlab.com/danielfutrille/toten-ui-test](https://gitlab.com/danielfutrille/toten-ui-test)


